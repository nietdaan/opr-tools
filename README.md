# OPR Tools
Userscript for [Ingress Operation Portal Recon](https://opr.ingress.com/recon)
> **Download:** https://gitlab.com/1110101/opr-tools/raw/master/opr-tools.user.js

![](./image/opr-tools.png)
![](./image/opr-tools-2.png)

## Features:
- Additional links to map services like Intel, OpenStreetMap, bing and some national ones
- Disabled annoying automatic page scrolling
- Automatically opens the first listed possible duplicate and the "What is it?" filter text box
- Buttons below the comments box to auto-type common 1-star rejection reasons
- Percent of total reviewed candidates processed
- Translate text buttons for title and description
- Moved overall portal rating to same group as other ratings
- Changed portal markers to small circles, inspied by IITC style
- Made "Nearby portals" list and map scrollable with mouse wheel
- **Keyboard navigation (Keys 1-5, Space to confirm, Esc, Shift, Tab for navigation + Keys on NUMPAD)**